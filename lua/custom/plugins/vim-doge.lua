vim.g.doge_enable_mappings = 0
vim.g.doge_doc_standard_cpp = 'doxygen_cpp_comment_exclamation'
vim.keymap.set('n', '<leader>cd', '<plug>(doge-generate)', { desc = 'Generate [C]ode [D]ocumentation' })
return {
	'kkoomen/vim-doge',
	run = ':call doge#install()'
}
